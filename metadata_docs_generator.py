#!/bin/python3
import os
import sys
from openpyxl import load_workbook

if not os.path.exists('docs'):
    os.makedirs('docs')

if len(sys.argv) > 1:
    excel_file = sys.argv[1]
else:
    print("No metadata excel file given")
    sys.exit(1,)

#wb = load_workbook(filename=excel_file)

try:
    wb = load_workbook(filename=excel_file)
except:
    print("Failed to read file: "+sys.argv[1]+ \
          "\nSupported formats are: .xlsx,.xlsm,.xltx,.xltm")
    sys.exit(1)

requirement = {"M": "Mandatory",
               "X": "Optional",
               "C": "Condition Specific",
               "H": "Hidden",
               "E": "Epple"
               }

mkdocs_yaml = open("mkdocs_nav.yml", "w")

for sheetname in wb.sheetnames:
    print(sheetname)
    sheet = wb[sheetname]
    rows = list(sheet.rows)
    package = ""
    mdfile = None

    mkdocs_yaml.write("- '"+sheetname+"':\n")
    columns = {}
    for i, cell in enumerate(sheet[1]):
        if cell.value:
            columns[i] = cell.value.strip("# ")

    for row in rows:
        for i, cell in enumerate(row):
            if str(cell.value)[0] == "#" or not cell.value:
                break

            if i == 0 and cell.value != package:
                package = cell.value
                mdfile_name = sheetname+"_"+package.replace("/", "-").replace(" ", "_")+".md"
                if mdfile:
                    mdfile.close()
                    mdfile = open("docs/"+mdfile_name, "w")
                else:
                    mdfile = open("docs/"+mdfile_name, "w")
                mkdocs_yaml.write("  - '" + package + "': '"+mdfile_name+"'\n")

            if not cell.value:
                cell.value = "-"

            if i == 1:
                mdfile.write("\n\n## "+str(cell.value)+"\n")
                mdfile.write("|||\n|---|---|\n|**Package**|" + package + "|"+"\n")
            elif cell.value != package:
                if i == 3:
                    mdfile.write("|**" + columns[i] + "**|" + str(requirement[cell.value]) + "|"+"\n")
                else:
                    mdfile.write("|**"+columns[i]+"**|"+str(cell.value).replace("|"," \| ")+"|"+"\n")
mdfile.close()
mkdocs_yaml.close()