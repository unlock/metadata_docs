## Unlock Metadata Field Descriptions

You can download the metadata excelsheet that this documentation has been generated from: [here](http://docs.m-unlock.nl/MetadataFields/unlock_metadata.xlsx)  
<br />
<br />
More Unlock E-Infrastructure documentation at [docs.m-unlock.nl](http://docs.m-unlock.nl)
<br />
<br />
What is Unlock? See general info at [m-unlock.nl](https://m-unlock.nl)