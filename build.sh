#!/bin/bash
#============================================================================
#title          :Metadata field documentation
#description    :Generate metadata field documentation from a metadata excelfile.
#author         :Bart Nijsse & Jasper Koehorst
#date           :2021
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# clean up
rm -rf site mkdocs.yml docs mkdocs_nav.yml

python3 metadata_docs_generator.py $1
exit_status=$?
if [ $exit_status -eq 0 ]; then
  # Copy the permanent files into docs
  cp permanent/index.md permanent/webform.md ./docs/

  # Merge the main website with the automatic generated content
  cat permanent/mkdocs_base.yml mkdocs_nav.yml > mkdocs.yml

  # Build the website
  mkdocs build
else
	echo "Stopped build script"
	exit 1
fi

